sprites.png
size: 112, 16
format: RGBA8888
filter: Nearest,Nearest
repeat: none
brick
  rotate: false
  xy: 0, 0
  size: 16, 16
  orig: 16, 16
  offset: 0, 0
  index: -1
plate
  rotate: false
  xy: 16, 0
  size: 16, 16
  orig: 16, 16
  offset: 0, 0
  index: -1
question
  rotate: false
  xy: 32, 0
  size: 16, 16
  orig: 16, 16
  offset: 0, 0
  index: -1
stone
  rotate: false
  xy: 48, 0
  size: 16, 16
  orig: 16, 16
  offset: 0, 0
  index: -1
coin
  rotate: false
  xy: 64, 0
  size: 16, 16
  orig: 16, 16
  offset: 0, 0
  index: -1
flag_top
  rotate: false
  xy: 80, 0
  size: 16, 16
  orig: 16, 16
  offset: 0, 0
  index: -1
flag_mid
  rotate: false
  xy: 96, 0
  size: 16, 16
  orig: 16, 16
  offset: 0, 0
  index: -1
