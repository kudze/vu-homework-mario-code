package com.kk.mario.rendering.text;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;

public class Font {
    private BitmapFont instance;

    public Font(FileHandle fontFile, int size)
    {
        this(fontFile, size, new Color(1, 1, 1, 1));
    }

    public Font(FileHandle fontFile, int size, Color color)
    {
        this(fontFile, size, color, new Color(0, 0, 0, 0), 0);
    }

    public Font(FileHandle fontFile, int size, Color color, Color borderColor, float borderSize)
    {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(fontFile);

        FreeTypeFontParameter parameter = new FreeTypeFontParameter();
        parameter.size = size;
        parameter.color = color;
        parameter.borderColor = borderColor;
        parameter.borderWidth = borderSize;

        this.instance = generator.generateFont(parameter);

        generator.dispose();
    }

    public void dispose() {
        this.instance.dispose();
    }

    public BitmapFont getInstance() {
        return instance;
    }
}
