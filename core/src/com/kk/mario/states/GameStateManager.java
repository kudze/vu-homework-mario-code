package com.kk.mario.states;

public class GameStateManager {
    private GameState activeGameState = null;

    public GameStateManager(GameState initialState)
    {
        this.activeGameState = initialState;
    }

    public GameState getActiveGameState() {
        return activeGameState;
    }

    public void dispose() {
        this.activeGameState.dispose();
    }

    public void setActiveGameState(GameState activeGameState) {
        GameState oldGameState = this.activeGameState;
        this.activeGameState = activeGameState;
        oldGameState.dispose();

        /*if(oldGameState != null) {
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {

            }

            //oldGameState.dispose();
        }*/
    }
}
