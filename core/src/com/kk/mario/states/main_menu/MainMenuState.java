package com.kk.mario.states.main_menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.kk.mario.MarioGame;
import com.kk.mario.rendering.image.Picture;
import com.kk.mario.rendering.text.Font;
import com.kk.mario.states.GameState;
import com.kk.mario.states.game.FirstLevel;
import com.kk.mario.states.game.SecondLevel;
import com.kk.mario.states.game.ThirdLevel;

public class MainMenuState implements GameState {
    private SpriteBatch batch;
    private Font bigFont;
    private Picture background;
    private Selection selection;

    public MainMenuState() {
        this.batch = new SpriteBatch();
        this.background = new Picture(Gdx.files.internal("img/main_menu_bg.jpg"), 1280, 720);
        this.bigFont = new Font(Gdx.files.internal("fonts/SuperMario256.ttf"), 100, Color.WHITE, Color.BLACK, 4);

        this.selection = new Selection(
                new Font(Gdx.files.internal("fonts/SuperMario256.ttf"), 50, Color.WHITE, Color.BLACK, 2),
                new Font(Gdx.files.internal("fonts/SuperMario256.ttf"), 50, Color.RED, Color.BLACK, 2)
        );

        this.selection.addOption(new Option("1st Level") {
            @Override
            public void execute() {
                MarioGame.getInstance().getGameStateManager().setActiveGameState(new FirstLevel());
            }
        });

        this.selection.addOption(new Option("2nd Level") {
            @Override
            public void execute() {
                MarioGame.getInstance().getGameStateManager().setActiveGameState(new SecondLevel());
            }
        });

        this.selection.addOption(new Option("3rd Level") {
            @Override
            public void execute() {
                MarioGame.getInstance().getGameStateManager().setActiveGameState(new ThirdLevel());
            }
        });

        this.selection.addOption(new Option("Quit") {
            @Override
            public void execute() {
                MarioGame.getInstance().dispose();
                Gdx.app.exit();
                System.exit(0);
            }
        });
    }

    @Override
    public void update() {
        this.selection.update();
    }

    @Override
    public void render() {

        batch.begin();
        this.background.getInstance().draw(batch, 1);
        this.bigFont.getInstance().draw(batch, "mario game", 50, 650);
        this.selection.render(batch, 50, 525);

        batch.end();
    }

    @Override
    public void dispose() {
        this.selection.dispose();
        this.bigFont.dispose();
        this.background.dispose();
        this.batch.dispose();
    }

}
