package com.kk.mario.states.game.map;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public class Stone extends Block {

    public Stone(TextureAtlas textureAtlas)
    {
        super(textureAtlas.createSprite("stone"), true);
    }

}
