package com.kk.mario.states.game.map;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.kk.mario.states.game.entities.mario.Mario;

public class Flag extends Block {

    public Flag(TextureAtlas textureAtlas, boolean top)
    {
        super(textureAtlas.createSprite(top ? "flag_top" : "flag_mid"), false);
    }

    public void onMarioTouch(Mario mario) {
        if(Math.abs(mario.getPosition().x - (this.getMapX() * 16)) > 0.5f)
            return;

        mario.setPosition(new Vector2(this.getMapX() * 16, mario.getPosition().y));
        mario.touchedFlag();
    };

}
