package com.kk.mario.states.game.entities.enemies;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.kk.mario.states.game.map.Block;

import java.util.ArrayList;

public class Smile extends Enemy {

    private Sprite smileDead;
    private boolean dead = false;

    private ArrayList<Sprite> smileWalkingFrames;
    private float walkingAnimTime = 0;

    public Smile(TextureAtlas enemyTextureAtlas, int spawnX, int spawnY) {
        super(16, 16, spawnX, spawnY);

        this.smileDead = enemyTextureAtlas.createSprite("smile_dead");
        this.smileWalkingFrames = new ArrayList<>();
        this.smileWalkingFrames.add(enemyTextureAtlas.createSprite("smile_walk_1"));
        this.smileWalkingFrames.add(enemyTextureAtlas.createSprite("smile_walk_2"));

        this.goLeft();
    }

    @Override
    public void render(Batch batch) {
        if (!this.dead) {
            this.walkingAnimTime += Gdx.graphics.getDeltaTime();
            int frame = ((int) Math.round(walkingAnimTime * 2)) % this.smileWalkingFrames.size();
            Sprite walkingSprite = this.smileWalkingFrames.get(frame);

            this.renderSprite(batch, walkingSprite);
        } else {
            this.renderSprite(batch, this.smileDead);
        }
    }

    @Override
    public void dispose() {

    }

    @Override
    protected void collidedWithBlock(Block block, HitDirection direction) {
        if (direction == HitDirection.LEFT)
            this.goRight();

        if (direction == HitDirection.RIGHT)
            this.goLeft();
    }

    public void goLeft() {
        Vector2 force = this.getForce();
        force.x = -30;
        this.setDirection(Direction.LEFT);
        this.setForce(force);
    }

    public void goRight() {
        Vector2 force = this.getForce();
        force.x = 30;
        this.setDirection(Direction.RIGHT);
        this.setForce(force);
    }

    @Override
    public void die() {
        this.dead = true;
        this.setForce(new Vector2());
    }

    @Override
    public boolean isDead() {
        return this.dead;
    }
}
