package com.kk.mario.states.game;

import com.kk.mario.states.game.entities.mario.Mario;
import com.kk.mario.states.game.map.*;

public class FirstLevel extends LevelState {
    public FirstLevel()
    {
        super(new Map(30, 12), new Mario(2, 8));

        for(int i = 0; i < 30; i++) {
            this.getMap().setBlockAtXY(
                    i,
                    11,
                    new Stone(this.getBlocksTextureAtlas())
            );

            if(i >= 17 || i <= 10)
            this.getMap().setBlockAtXY(
                    i,
                    0,
                    new Stone(this.getBlocksTextureAtlas())
            );
        }

        for(int i = 0; i < 12; i++) {
            this.getMap().setBlockAtXY(
                    0,
                    i,
                    new Stone(this.getBlocksTextureAtlas())
            );

            this.getMap().setBlockAtXY(
                    29,
                    i,
                    new Stone(this.getBlocksTextureAtlas())
            );
        }

        this.getMap().setBlockAtXY(4, 4, new Question(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(5, 4, new Question(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(6, 4, new Question(this.getBlocksTextureAtlas()));

        this.getMap().setBlockAtXY(10, 4, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(10, 3, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(10, 2, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(10, 1, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(11, 4, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(12, 4, new Brick(this.getBlocksTextureAtlas()));

        this.getMap().setBlockAtXY(10, 5, new Coin(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(11, 5, new Coin(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(12, 5, new Coin(this.getBlocksTextureAtlas()));

        this.getMap().setBlockAtXY(10, 7, new Question(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(11, 7, new Question(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(12, 7, new Question(this.getBlocksTextureAtlas()));

        this.getMap().setBlockAtXY(17, 4, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(17, 3, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(17, 2, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(17, 1, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(16, 4, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(15, 4, new Brick(this.getBlocksTextureAtlas()));

        this.getMap().setBlockAtXY(15, 7, new Question(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(16, 7, new Question(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(17, 7, new Question(this.getBlocksTextureAtlas()));

        this.getMap().setBlockAtXY(15, 5, new Coin(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(16, 5, new Coin(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(17, 5, new Coin(this.getBlocksTextureAtlas()));

        this.getMap().setBlockAtXY(20, 5, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(21, 5, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(22, 5, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(23, 5, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(24, 5, new Brick(this.getBlocksTextureAtlas()));

        this.getMap().setBlockAtXY(27, 1, new Stone(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(27, 2, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(27, 3, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(27, 4, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(27, 5, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(27, 6, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(27, 7, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(27, 8, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(27, 9, new Flag(this.getBlocksTextureAtlas(), true));
    }
}
