package com.kk.mario.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.kk.mario.MarioGame;

public class DesktopLauncher {

	public static void main (String[] arg) {
		Window window = new Window ("Mario | Karolis Kraujelis",1280, 720, false);
		window.startApplication(MarioGame.getInstance());
	}

}
